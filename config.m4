dnl config.m4
PHP_ARG_WITH(PhpExcel)
PHP_ARG_ENABLE(PhpExcel, whether to enable PhpExcel support,
dnl Make sure that the comment is aligned:
[  --enable-PhpExcel           Enable PhpExcel support])

if test "$PHP_PHPEXCEL" != "no"; then
	dnl EXTRA_CXXFLAGS = "-std=c++11" 
	PHP_REQUIRE_CXX()
	PHP_ADD_INCLUDE(./include)
	PHP_ADD_LIBRARY_WITH_PATH(xl, ./ext_lib, PHPEXCEL_SHARED_LIBADD)

	PHP_SUBST(PHPEXCEL_SHARED_LIBADD)
	dnl PHP_NEW_EXTENSIDDON(PhpExcel, PhpExcel.cpp, $ext_shared,, -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1)
	PHP_NEW_EXTENSION(PhpExcel, PhpExcel.cpp, $ext_shared)
fi

