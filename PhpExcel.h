#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"

/*
Author: Yax 
uRL: https://github.com/zydp
*/ 

ZEND_BEGIN_MODULE_GLOBALS(PhpExcel)
//globals

ZEND_END_MODULE_GLOBALS(PhpExcel)

extern zend_module_entry PhpExcel_module_entry;
#define phpext_PhpExcel_ptr &PhpExcel_module_entry

#define PHP_PHPEXCEL_VERSION "1.0.0" /* Replace with version number for your extension */

#ifdef PHP_WIN32
#	define PHP_PHPEXCEL_API __declspec(dllexport)
#elif defined(__GNUC__) && __GNUC__ >= 4
#	define PHP_PHPEXCEL_API __attribute__ ((visibility("default")))
#else
#	define PHP_PHPEXCEL_API
#endif

#ifdef ZTS
#include "TSRM.h"
#endif

#define PHPEXCEL_G(v) ZEND_MODULE_GLOBALS_ACCESSOR(PhpExcel, v)

#if defined(ZTS) && defined(COMPILE_DL_PHPEXCEL)
	ZEND_TSRMLS_CACHE_EXTERN()
#endif



